package ru.dukentre.dukentrenotforgot.network

object ApiUtils {
    const val BASE_URL = "https://apinotforgot.herokuapp.com";

    val apiService: ApiService
        get() = RetrofitClient.getClient(BASE_URL)!!
            .create(ApiService::class.java)
}