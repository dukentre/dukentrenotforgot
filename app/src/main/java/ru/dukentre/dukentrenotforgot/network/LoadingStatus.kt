package ru.dukentre.dukentrenotforgot.network

enum class LoadingStatus {
    RUNNING,
    SUCCESS,
    FAILED,
}