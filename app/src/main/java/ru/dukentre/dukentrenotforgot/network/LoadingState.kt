package ru.dukentre.dukentrenotforgot.network

data class LoadingState private  constructor(val status: LoadingStatus,val isRefreshing:Boolean = false, val message: String? = null){
    companion object{
        val LOADED = LoadingState(LoadingStatus.SUCCESS,false)
        val LOADING = LoadingState(LoadingStatus.RUNNING,false)

        val REFRESHING = LoadingState(LoadingStatus.RUNNING,true)


        fun error(message: String?) = LoadingState(LoadingStatus.FAILED,false,message)
    }
}