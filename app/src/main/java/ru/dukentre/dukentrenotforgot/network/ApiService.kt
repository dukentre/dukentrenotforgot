package ru.dukentre.dukentrenotforgot.network

import android.app.TaskInfo
import retrofit2.http.GET
import ru.dukentre.dukentrenotforgot.adapter.GroupsAdapter

interface ApiService {
    @GET("tasks")
    suspend fun getTasksAsync(): ArrayList<Tasks>
}