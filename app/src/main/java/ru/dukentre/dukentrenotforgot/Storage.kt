package ru.dukentre.dukentrenotforgot

import android.graphics.Color
import ru.dukentre.dukentrenotforgot.adapter.GroupsAdapter
import ru.dukentre.dukentrenotforgot.adapter.NotesAdapter

object Storage {
    enum class Indicator(val value: Int) {
        Red(Color.parseColor("#fc4040")),
        Green(Color.parseColor("#64c16d")),
        Blue(Color.parseColor("#6083fe")),
        Yellow(Color.parseColor("#ffc453")),
    }

    fun createGroups(): ArrayList<GroupsAdapter.Group> {
        return arrayListOf(
            GroupsAdapter.Group("Дукентр", arrayListOf(
                NotesAdapter.Note("Дукентра", "Дукентр", Indicator.Red.value, true),
                NotesAdapter.Note("Дукентр Дукентр Дукентр", "Дукентр Дукентр Дукентр", Indicator.Blue.value, false),
            )),
        )
    }
}