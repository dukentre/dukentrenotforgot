package ru.dukentre.dukentrenotforgot.activity

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ru.dukentre.dukentrenotforgot.GroupModel
import ru.dukentre.dukentrenotforgot.adapter.GroupsAdapter
import ru.dukentre.dukentrenotforgot.databinding.ActivityMainBinding
import ru.dukentre.dukentrenotforgot.network.LoadingStatus

class MainActivity : AppCompatActivity() {
    private lateinit var L: ActivityMainBinding
    private lateinit var adapter: GroupsAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        L = ActivityMainBinding.inflate(layoutInflater)
        setContentView(L.root)

        val model = ViewModelProvider(this).get(GroupModel::class.java)
            .observe(this, { observe(it) })
        val groups = model.groups
        L.rvGroups.layoutManager = LinearLayoutManager(this)
        adapter = GroupsAdapter(groups)
        L.rvGroups.adapter = adapter
        L.rvGroups.addOnScrollListener(onScrollListener)
        L.fab.setOnClickListener { fabClick() }

        val callback = adapter.ItemTouchCallback()
        val swipeHelper = ItemTouchHelper(callback)
        swipeHelper.attachToRecyclerView(L.rvGroups)


        model.states.observe(this, { currentState ->
            when (currentState.status) {
                LoadingStatus.FAILED -> {
                    Toast.makeText(this, currentState.message, Toast.LENGTH_LONG).show()
                    hideProgress(!currentState.isRefreshing)
                }
                LoadingStatus.RUNNING -> {
                    showProgress(!currentState.isRefreshing)
                }
                LoadingStatus.SUCCESS -> {
                    hideProgress(!currentState.isRefreshing)
                }
            }
        })

        L.refreshTasks.setOnRefreshListener {
            GlobalScope.launch(Dispatchers.IO) {
                model.fetchTasks(true);
            }
        }
    }

    private fun observe(groups: ArrayList<GroupsAdapter.Group>) {
        if (this::adapter.isInitialized) {
            adapter.updateList(groups)
        }
    }

    private fun fabClick() {
        val intent = Intent(this, AddTaskActivity::class.java)
        startActivity(intent)
    }


    private val onScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            if (dy > 0 && L.fab.isShown) {
                L.fab.hide()
            } else if (dy < 0 && !L.fab.isShown) {
                L.fab.show()
            }
        }
    }

    fun hideProgress(withProgressBar: Boolean = true) {
        if(withProgressBar) {
            L.progressLoading.isVisible = false;
        }
        L.rvGroups.isVisible = true;
        L.refreshTasks.isRefreshing = false;
    }

    fun showProgress(withProgressBar: Boolean = true) {
        if(withProgressBar) {
            L.progressLoading.isVisible = true;
        }
        L.rvGroups.isVisible = false;
    }
}