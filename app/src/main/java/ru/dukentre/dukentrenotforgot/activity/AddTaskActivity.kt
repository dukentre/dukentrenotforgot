package ru.dukentre.dukentrenotforgot.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import ru.dukentre.dukentrenotforgot.databinding.ActivityAddTaskBinding

class AddTaskActivity : AppCompatActivity() {
    private lateinit var L: ActivityAddTaskBinding;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        L = ActivityAddTaskBinding.inflate(layoutInflater)
        setContentView(L.root)
    }
}