package ru.dukentre.dukentrenotforgot

import android.graphics.Color
import androidx.lifecycle.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ru.dukentre.dukentrenotforgot.adapter.GroupsAdapter
import ru.dukentre.dukentrenotforgot.adapter.NotesAdapter
import ru.dukentre.dukentrenotforgot.network.ApiUtils
import ru.dukentre.dukentrenotforgot.network.LoadingState
import java.lang.Exception

class GroupModel : ViewModel() {

    private var groupsObject = MutableLiveData<ArrayList<GroupsAdapter.Group>>()
    val states: MutableLiveData<LoadingState> = MutableLiveData();
    init {
        GlobalScope.launch(Dispatchers.IO) {
            fetchTasks();
        }
    }

    var groups: ArrayList<GroupsAdapter.Group>
        get() {
            if (groupsObject.value == null) {
                groupsObject.value = ArrayList()
            }
            return groupsObject.value!!
        }
        set(value) {
            groupsObject.postValue(value)
        }

    fun observe(owner: LifecycleOwner, observer: Observer<ArrayList<GroupsAdapter.Group>>): GroupModel {
        groupsObject.observe(owner, observer)
        return this
    }

    suspend fun fetchTasks(isRefresh:Boolean = false){
        try {
            if(isRefresh){
                states.postValue(LoadingState.REFRESHING)
            }else {
                states.postValue(LoadingState.LOADING)
            }
            val tasks = ApiUtils.apiService.getTasksAsync()
            val categories = hashMapOf<String, Int>()
            val groups = arrayListOf<GroupsAdapter.Group>()
            tasks.forEach {
                if (!categories.containsKey(it.category.name)) {
                    groups.add(GroupsAdapter.Group(it.category.name, ArrayList()))
                    categories[it.category.name] = groups.lastIndex
                }
                groups[categories[it.category.name]!!].notes.add(
                    NotesAdapter.Note(
                        it.name,
                        it.description,
                        Color.parseColor(it.priority.color),
                        it.done > 0
                    )
                )
            }
            this@GroupModel.groups = groups

            states.postValue(LoadingState.LOADED)
        }
        catch (ex: Exception){
            states.postValue(LoadingState.error(ex.message));
        }
    }
    //val groups = Storage.createGroups();
}